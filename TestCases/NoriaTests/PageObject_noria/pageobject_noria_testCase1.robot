*** Settings ***
Library    Selenium2Library  
Library    OperatingSystem    
Library    XML    
Resource    ../Conf_noria/conf_noria_testCase1.robot  

*** Keywords ***

!LoginGassi
    Open Browser    https://gassi.si.francetelecom.fr/    firefox
    Maximize Browser Window
    Set Browser Implicit Wait    10
    Wait Until Element Is Visible    xPath=/html/frameset/frame[2]    20
    Select Frame    xPath=/html/frameset/frame[2]
    Wait Until Element Is Visible    id=user    10
    Input Text    id=user    LGCX2586
    Wait Until Element Is Visible    id=password    10
    Input Text    id=password    lgcx2586
    Click Link    id=linkValidForm
    Select Window    Information sur le Mot de Passe
    Close Window

!AccesNoria
    Select Window    Mon SI : l'accueil dans le Système d'Information
    Wait Until Element Is Visible    xPath=/html/frameset/frame[2]    20 
    Select Frame    xPath=/html/frameset/frame[2]
    Wait Until Element Is Visible    xPath=//*[@id="tabapplis"]/tbody/tr[2]/td[2]/a
    Click Link    xPath=//*[@id="tabapplis"]/tbody/tr[2]/td[2]/a
    
!RechercheDemandeADDOO
    Set Browser Implicit Wait    100
    Select Window    NORIA   
    Wait Until Element Is Visible    xPath=//*[@id="menu-top"]/ul/li[2]/a   
    Click Link    xPath=//*[@id="menu-top"]/ul/li[2]/a 
    Click Element    xPath=//*[@id="typeSearchSelectContainer"]/button/span[1]   
    Wait Until Element Is Enabled    id=SearchTypeSelect
    Click Element    xPath=/html/body/ul/li[2]/a
    
    ${FILE_CONTENT}=    Parse Xml    operations.xml
    ${NUM_DEMANDE}=    Get Element Text    ${FILE_CONTENT}    xpath=operation[@id="1"]/general/num_demande
    Input Text    id=gs_numAddooSearch    ${NUM_DEMANDE}
    
    Wait Until Element Is Enabled    id=SearchAddooButton
    Click Button    id=SearchAddooButton
    Click Element    xPath=//*[@id="IndexSearchAddoo"]/tbody/tr[2]
    Wait Until Element Is Enabled    id=DetailsAddooButton    
    Click Button    id=DetailsAddooButton
    Set Browser Implicit Wait    20
    
    [Documentation]    Recherche de demandes par filtres :
    #Click Button    xPath=//*[@id="gview_IndexSearchAddoo"]/div[2]/div/table/thead/tr[2]/th[5]/div/button
    #Click Element    id=ui-multiselect-gs_uprAddooSearch-option-8
    #Click Button    xPath=//*[@id="gview_IndexSearchAddoo"]/div[2]/div/table/thead/tr[2]/th[8]/div/button
    #Click Element    xPath=/html/body/div[5]/ul/li[59]/label
    #Click Button    xPath=//*[@id="gview_IndexSearchAddoo"]/div[2]/div/table/thead/tr[2]/th[9]/div/button
    #Click Element    xPath=/html/body/div[6]/ul/li[3]/label/span
    
!RechercheDemandeADDOOExistante 
    Set Browser Implicit Wait    100
    Select Window    NORIA   
    Wait Until Element Is Visible    xPath=//*[@id="menu-top"]/ul/li[2]/a   
    Click Link    xPath=//*[@id="menu-top"]/ul/li[2]/a 
    Click Element    xPath=//*[@id="typeSearchSelectContainer"]/button/span[1]   
    Wait Until Element Is Enabled    id=SearchTypeSelect
    Click Element    xPath=/html/body/ul/li[2]/a 
    
    ${FILE_CONTENT}=    Parse Xml    operations.xml
    ${NUM_DEMANDE}=    Get Element Text    ${FILE_CONTENT}    xpath=operation[@id="1"]/general/num_demande
    Input Text    id=gs_numAddooSearch    ${NUM_DEMANDE}
    
    Wait Until Element Is Enabled    id=SearchAddooButton
    Click Button    id=SearchAddooButton
    Click Element    xPath=//*[@id="IndexSearchAddoo"]/tbody/tr[2]
    Wait Until Element Is Enabled    id=DetailsAddooOpButon    
    Click Button    id=DetailsAddooOpButon
    Set Browser Implicit Wait    20
    
!ModifierDemande
    
    ${XML_REF}=    Parse Xml    operations.xml
    ${ADRESSE}=    Get Element Text    ${XML_REF}    operation[@id="1"]/noria/adresse
    
    Click Button    xPath=//*[@id="groupeDeNoeudsProcess_datas"]/table/tbody/tr[1]/td[6]/button
    Click Element    xPath=/html/body/ul[1]/li[76]/a
    Click Button    id=buttonCreerSiteTheorique
    Sleep    2
    Input Text    id=rechercheAdresse    ${ADRESSE}
    Wait Until Element Is Visible    id=validerSite    
    Click Button    id=validerSite
    Sleep    2
    ${BUTTON_SITE_PROX}=    Run Keyword And Return Status    Page Should Contain Button    id=selectionnerSiteProximite    
    Log    ${BUTTON_SITE_PROX}
    Run Keyword If    ${BUTTON_SITE_PROX} == True    Click Button    id=selectionnerSiteProximite
    Sleep    2
    Wait Until Element Is Enabled    id=valider    20
    Click Button    id=valider
    Set Browser Implicit Wait    100
    Sleep    4
    
!TraitementOperation   
    Set Browser Implicit Wait    100
    Wait Until Element Is Visible    id=detailOperation
    Click Button    id=detailOperation
    Wait Until Element Is Visible    id=retour    
    Click Button    id=retour    
    
   
    
    
