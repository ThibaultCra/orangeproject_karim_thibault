*** Settings ***
Library    Selenium2Library    
Resource    ../PageObject_noria/pageobject_noria_testCase1.robot

*** Keywords ***

OperationNouvelle
    !LoginGassi
    !AccesNoria
    !RechercheDemandeADDOO
    !ModifierDemande
     
OperationExistante
    !LoginGassi
    !AccesNoria
    !RechercheDemandeADDOOExistante
    !TraitementOperation

