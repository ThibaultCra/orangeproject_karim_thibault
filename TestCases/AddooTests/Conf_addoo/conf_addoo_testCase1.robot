*** Settings ***
Library    XML

*** Variables ***
#${URL_ADDOO}    http://dvltewsc06-adm.rouen.francetelecom.fr/
${URL_ADDOO}      https://www.google.com/

*** Keywords ***

RecupereVariables
    ${XML_REF}=    Parse Xml    operations.xml
    ${DR}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/dr
    ${NOM_DEMANDE}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/nom_demande
    ${UPR}=    Get Element Text    ${XML_REF}    operation[@id="1"]/general/upr
    ${TYPE_OP}=    Get Element Text    ${XML_REF}    operation[@id="1"]/general/type
    ${X}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/mkt/x
    ${Y}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/mkt/y
    ${ZONE_DE_VIE}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/mkt/zone_de_vie
    ${MORPHOLOGIE}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/mkt/morphologie
    ${PHASE}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/mkt/phase
    ${BLOC}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/mkt/bloc
    ${ZONE_MKT}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/mkt/zone_mkt
    ${OP}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/gsm/op
    ${BANDE}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/gsm/bandes
    ${MKT1}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/gsm/mkt1
    ${MKT2}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/gsm/mkt2
    ${PRIORITE}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/gsm/prio
    ${ORIGINE}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/gsm/origine
        



