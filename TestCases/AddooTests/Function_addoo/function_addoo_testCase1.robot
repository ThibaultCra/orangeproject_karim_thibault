*** Settings ***
Resource    ../PageObject_addoo/pageobject_addoo_testCase1.robot

*** Keywords ***

MiseEnPlace
    !AccesAddoo
    !AccesDemande
    
RemplirFormulaire
    !RemplirOperations
    !RemplirLocalisationMKT
    !RemplirDetailsGSM
    #!Valider
    !RecupNumeroDemande