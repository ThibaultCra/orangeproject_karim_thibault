*** Settings ***
Library    Selenium2Library 
Library    OperatingSystem 
Library    String
Library    XML
Resource    ../Conf_addoo/conf_addoo_testCase1.robot   

*** Keywords ***

!AccesAddoo
    Open Browser    ${URL_ADDOO}    ff
    Maximize Browser Window
    Set Browser Implicit Wait    5
    Select From List By Index    name=UR    0
    Select From List By Index    name=LISTE_N    83    
    Click Button    xPath=/html/body/center/form/table/tbody/tr[4]/td[2]/input[2]  
    
!AccesDemande
    
    ${XML_REF}=    Parse Xml    operations.xml
    ${DR}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/dr
    ${NOM_DEMANDE}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/nom_demande
    ${UPR}=    Get Element Text    ${XML_REF}    operation[@id="1"]/general/upr
    
    Wait Until Element Is Visible    xPath=/html/body/table/tbody/tr[2]/td[1]/font/table[8]/tbody/tr/td/a
    Click Element    xPath=/html/body/table/tbody/tr[2]/td[1]/font/table[8]/tbody/tr/td/a
    Select From List By Label    name=ur_filter    ${UPR}       
    Wait Until Element Is Visible    xPath=/html/body/table/tbody/tr[2]/td[1]/font/table[8]/tbody/tr/td/a
    Click Element    xPath=/html/body/table/tbody/tr[2]/td[1]/font/table[8]/tbody/tr/td/a
    Input Text    name=SITE_NOM_DEMANDE    ${NOM_DEMANDE}
    Click Element    name=SITE_DR
    Select From List By Label    name=SITE_DR    ${DR}
    
!RemplirOperations
    
    ${XML_REF}=    Parse Xml    operations.xml
    ${TYPE_OP}=    Get Element Text    ${XML_REF}    operation[@id="1"]/general/type
    
    Wait Until Element Is Enabled    name=SITE_NUM_TYPE_OP    
    Select From List By Label    name=SITE_NUM_TYPE_OP    ${TYPE_OP}
    
!RemplirLocalisationMKT
    
    ${XML_REF}=    Parse Xml    operations.xml
    ${X}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/mkt/x
    ${Y}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/mkt/y
    ${ZONE_DE_VIE}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/mkt/zone_de_vie
    ${MORPHOLOGIE}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/mkt/morphologie
    ${PHASE}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/mkt/phase
    ${BLOC}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/mkt/bloc
    ${ZONE_MKT}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/mkt/zone_mkt
    
    Input Text    name=SITE_CODE_CONTOUR_MKT    013Z1014
    Input Text    name=SITE_X    ${X}
    Input Text    name=SITE_Y    ${Y}     
    Wait Until Element Is Visible    name=SITE_MORPHOLOGIE
    Select From List By Label    name=SITE_MORPHOLOGIE    ${MORPHOLOGIE}      
    Wait Until Element Is Visible    name=SITE_PHASE    
    Select From List By Label    name=SITE_PHASE    ${PHASE}
    Wait Until Element Is Visible    name=SITE_BLOC
    Select From List By Label    name=SITE_BLOC    ${BLOC}
    Click Button    xPath=//*[@id="mkt"]/table/tbody/tr[6]/td[2]/input[2]
    Sleep    3
    Select Window    Choix pour le champ 'Zone de vie'
    Input Text    name=filter    ${ZONE_DE_VIE}
    Wait Until Element Is Visible    xPath=/html/body/center/form/input[2]
    Click Button    xPath=/html/body/center/form/input[2]
    Select Window    Bienvenue sur AddooNet ADRIEN LEVY - Siège/Siège
    Wait Until Element Is Visible    name=SITE_Z_MKT
    Select From List By Label    name=SITE_Z_MKT     ${ZONE_MKT}
    
!RemplirDetailsGSM
    
    ${XML_REF}=    Parse Xml    operations.xml
    ${OP}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/gsm/op
    ${BANDE}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/gsm/bandes
    ${MKT1}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/gsm/mkt1
    ${MKT2}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/gsm/mkt2
    ${PRIORITE}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/gsm/prio
    ${ORIGINE}=    Get Element Text    ${XML_REF}    operation[@id="1"]/addoo/gsm/origine
    
    Select From List By Label    name=GSM_OPERATION    ${OP}
    Select From List By Label    name=GSM_BANDES    ${BANDE}
    Select From List By Label    name=GSM_TYPAGE_MKT1    ${MKT1}
    Select From List By Label    name=GSM_TYPAGE_MKT2    ${MKT2}
    Select From List By Label    name=GSM_PRIORITE_DEC    ${PRIORITE}
    Select From List By Label    name=GSM_PROCESS_ORG    ${ORIGINE}
    Input Text    name=GSM_OBJECTIF    Test
    
!Valider
    [Documentation]    TODO : Validation a faire, bouton ajouter
    Close Browser
    
!RecupNumeroDemande
    
    ${NUM_DEMANDE_BRUT}=    Get Text    xpath=/html/body/table/tbody/tr[2]/td[2]/form/table[1]/tbody/tr[1]/th
    ${NUM_DEMANDE_PARSE}=    Get Substring    ${NUM_DEMANDE_BRUT}    10 
    ${EDIT_XML}=    Parse Xml    operations.xml
    Set Element Text    ${EDIT_XML}    ${NUM_DEMANDE_PARSE}    xpath=operation[@id="1"]/general/num_demande
    Save Xml    ${EDIT_XML}    operations.xml
    
    

    




