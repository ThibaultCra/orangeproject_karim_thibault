import inspect

from SeleniumLibrary import SeleniumLibrary


__version__ = '3.0.0'


class Selenium2Library(SeleniumLibrary):

    ROBOT_LIBRARY_VERSION = __version__

    def get_keyword_documentation(self, name):
        if name != '__intro__':
            doc = SeleniumLibrary.get_keyword_documentation(self, name)
            return doc.replace('SeleniumLibrary', 'Selenium2Library')
        intro = inspect.getdoc(SeleniumLibrary)
        intro = intro.replace('SeleniumLibrary', 'Selenium2Library')
        return """
---
*NOTE:* Selenium2Library has been renamed to SeleniumLibrary since version 3.0.
Nowadays Selenium2Library is just a thin wrapper to SeleniumLibrary that eases
with transitioning to the new project. See
[https://github.com/robotframework/SeleniumLibrary|SeleniumLibrary] and
[https://github.com/robotframework/Selenium2Library|Selenium2Library]
project pages for more information.
---
""" + intro
