*** Settings ***
Library    Selenium2Library  
Library    OperatingSystem    
Library    XML    
Resource    ../Conf_bodega/conf_bodega_testCase1.robot 
Resource    ../../NoriaTests/PageObject_noria/pageobject_noria_testCase1.robot

*** Keywords ***

!Login
    pageobject_noria_testCase1.!LoginGassi
    
!AccesBodega
    Select Window    Mon SI : l'accueil dans le Système d'Information
    Set Browser Implicit Wait    20
    Wait Until Element Is Visible    name=bgmainframe    20
    Select Frame    name=bgmainframe
    Wait Until Element Is Visible    xPath=//*[@id="tabapplis"]/tbody/tr[3]/td[2]/a
    Click Link    xpath=//*[@id="tabapplis"]/tbody/tr[3]/td[2]/a

!AccesOperations
    Set Browser Implicit Wait    20
    Select Window    ClickOnSite
    Set Browser Implicit Wait    20
    Mouse Over    xPath=//*[@id="main_menu"]/ul/li[3]/a
    Wait Until Element Is Visible    xPath=//*[@id="main_menu"]/ul/li[3]/div/ul[1]/li[2]/a    10
    Click Link    xPath=//*[@id="main_menu"]/ul/li[3]/div/ul[1]/li[2]/a
    
    Wait Until Element Is Visible    name=body    10
    Select Frame    name=body
     
    #Wait Until Element Is Visible    xpath=//*[@id="filter_fieldset"]/table/tbody/tr[4]/td[5]/input    10
    #Input Text    xpath=//*[@id="filter_fieldset"]/table/tbody/tr[4]/td[5]/input    00084016S14-18    #TODO : Variable XML à mettre !
    #Input Text    id=input_keyword    00084016S14-18
    
    

#!RendreDemandeReelle
    