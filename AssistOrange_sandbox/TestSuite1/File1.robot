*** Settings ***
Library    Selenium2Library   
Library    XML    
Library    OperatingSystem       
Suite Setup    Log    i am inside test suite setup
Suite Teardown    Log    i am inside test suite teardown
Test Setup    Log    i am inside test setup
Test Teardown    Log    i am inside test teardown
Default Tags    sanity

*** Test Cases ***

CreateAndEditXML
    CreateXML
    EditXML

SandboxXML
    ${TEST_XML}=    Parse Xml    agents.xml
    ${msg}=    Get Element    ${TEST_XML}    personne[@id="3"]/nom
    Log    ${msg}
    ${CONTENT}=    Get Element Text    ${TEST_XML}    personne[@id="2"]/service
    Log    ${CONTENT}
    
    Add Element    ${TEST_XML}    <gob2></gob2>
    Set Element Text    ${TEST_XML}    wolololo    xpath=gob2
    XML.Element Text Should Be   ${TEST_XML}    wolololo    xpath=gob2
    ${CONTENT_2}=    Get Element Text    ${TEST_XML}    gob2
    Log    ${CONTENT_2}
    
    Save Xml    ${TEST_XML}    agents.xml

DemoSearchAndLogin
    [Documentation]    Test de recherche Google et de Login
    Open Browser    ${URL_GOOGLE}    chrome
    Set Browser Implicit Wait    9
    Maximize Browser Window
    Sleep    2
    Input Text    name=q    ${TEXT_INPUT}
    Sleep    2
    Wait Until Element Is Visible    ${BUTTON_SEARCH}
    Click Button    ${BUTTON_SEARCH}    
    Sleep    2  
    Wait Until Element Is Visible    ${ORANGE_HRM_LINK}
    Click Element    ${ORANGE_HRM_LINK}
    Sleep    2
    LoginHRM
    Mouse Over    id=menu_admin_viewAdminModule
    Sleep    2
    Mouse Over    id=menu_admin_Job
    Wait Until Element Is Visible    id=menu_admin_jobCategory
    Click Element    id=menu_admin_jobCategory
    Sleep    2    
    Click Element    id=welcome
    Sleep    2
    Click Element    link=Logout
    Sleep    2
    Close Browser
    Log    Test completed
    
TestConsole
    Log To Console    Hello world    
    
    
*** Variables ***
${URL_GOOGLE}    https://google.com
${TEXT_INPUT}    orange hrm live login panel
${BUTTON_SEARCH}    xpath=//*[@id="tsf"]/div[2]/div/div[3]/center/input[1]
${ORANGE_HRM_LINK}    xpath=//*[@id="rso"]/div[1]/div/div[1]/div/div/div[1]/a[1]/h3
@{CREDENTIALS}    Admin    admin123
&{LOGINDATA}    username=Admin    password=admin123

*** Keywords ***
LoginHRM
    Sleep    2
    Input Text    id=txtUsername    @{CREDENTIALS}[0]
    Sleep    2
    Input Text    id=txtPassword    &{LOGINDATA}[password]
    Sleep    2
    Click Button    id=btnLogin
    
CreateXML
    Create File    test.xml    <?xml version='1.0' encoding='UTF-8'?><operations></operations>
    ${TEST_XML}=    Parse Xml    test.xml
    Add Element    ${TEST_XML}    <operation id="1"></operation>
    Add Element    ${TEST_XML}    <type_operation></type_operation>    xpath=operation  
    Add Element    ${TEST_XML}    <num_demande></num_demande>    xpath=operation
    Save Xml    ${TEST_XML}    test.xml
    
EditXML
    ${EDIT_XML}=    Parse Xml    test.xml
    Set Element Text    ${EDIT_XML}    Amenagement    xpath=operation/type_operation
    Save Xml    ${EDIT_XML}    test.xml
